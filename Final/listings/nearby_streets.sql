SELECT DISTINCT astext(transform(p.way, 4326)), l.name, p.name, st_distance(p.way, l.way) from planet_osm_point p join planet_osm_roads l ON st_dwithin(p.way, l.way, 50) WHERE p.name IS NOT NULL AND p.amenity='parking' ORDER BY st_distance(p.way, l.way) ASC LIMIT 100;

