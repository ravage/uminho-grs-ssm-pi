\o p1.txt

SELECT name, st_x(transform(st_pointonsurface(way), 4326)), st_y(transform(st_pointonsurface(way), 4326)) FROM planet_osm_polygon WHERE amenity='parking';

\o p2.txt

SELECT name, st_x(transform(st_pointonsurface(way), 4326)), st_y(transform(st_pointonsurface(way), 4326)) FROM planet_osm_line WHERE amenity='parking';

\o p3.txt

SELECT name, st_x(transform(way, 4326)), st_y(transform(way, 4326)) FROM planet_osm_point WHERE amenity='parking';
