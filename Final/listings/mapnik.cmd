$> svn export http://svn.openstreetmap.org/applications/rendering/mapnik

$> ./generate_xml.py --host localhost --dbname gis --symbols ../symbols/ --world_boundaries ../world_boundaries/

$> MAPNIK_MAP_FILE="osm.xml" MAPNIK_TILE_DIR="tiles/" ./generate_tiles.py
