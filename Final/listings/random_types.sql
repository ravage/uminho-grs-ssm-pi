CREATE OR REPLACE FUNCTION random_types() RETURNS VOID AS $$
DECLARE
  r RECORD;
  i int;
  rnd int;
  max_types int;
BEGIN
  SELECT MAX(id) FROM types INTO max_types;
  FOR r IN EXECUTE 'SELECT id FROM parks' LOOP
    FOR i IN 1..3 LOOP
      rnd := random(1, max_types)::int;
      WHILE EXISTS (SELECT * FROM parks_types WHERE park_id = r.id AND type_id = rnd) LOOP
        rnd := random(1, max_types)::int;
      END LOOP;
      INSERT INTO parks_types (park_id, type_id) VALUES (r.id, rnd);
    END LOOP;
  END LOOP;
END
$$ LANGUAGE plpgsql;
