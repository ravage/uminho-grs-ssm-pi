\chapter{Mapping Module}
\label{chap:modulos}

Nesta secção descreve-se o funcionamento do \emph{Mapping Module} a partir de uma perspetiva \emph{inside out}, onde inicialmente se define os serviços internos e respetiva interação. Os serviços internos definem-se como qualquer parte concebida, contida e gerida dentro do ecossistema \emph{Parking Search}, sendo os serviços externos, qualquer serviço alheio ao conceito \emph{Parking Search}, mas que fornece informação necessária para o bom funcionamento do mesmo.

\section{Definição do Módulo} % (fold)
\label{sec:definicao_modulo}

O \emph{Mapping Module} trabalha no sentido de fornecer aos serviços internos uma \ac{API} completamente focada na geo-referenciação. Fornece ao sistema todo o tipo de informação geográfica necessária ao seu funcionamento e serve como \emph{proxy} de acesso a serviços de geo-referenciação. Dentro deste contexto o módulo constitui uma camada de abstração especializada em serviços geográficos com uma \ac{API} consistente e possibilidade de expansão.

Enumera-se então o conjunto de tarefas delegadas ao módulo, todas elas em torno da geo-referenciação:

\begin{itemize}
  \item Gerar imagem com pontos de interesse para determinada área
  \item Conversão entre diferentes sistemas de coordenadas
  \item Calcular distâncias entre pontos
  \item Identificar pontos de interesse dentro de um raio
  \item Decifrar direções entre dois pontos
  \item Proxy de acesso a diferentes serviços externos
  \item Ficheiros de integração para aplicações geográficas
\end{itemize}

O \emph{Mapping Module} tem também como tarefa, requisitar a serviços externos ou internos, informação geográfica sobre pontos específicos. Neste contexto os pontos significam locais de interesse, nomeadamente parques de estacionamento. O tratamento contínuo desta informação permite ao módulo disponibilizar, para os serviços internos, dados geográficos prontos a consumir, como por exemplo, imagens estáticas marcadas com os respetivos pontos de interesse (Figura ~\ref{fig:staticmap}).

A utilização de serviços externos ou internos por parte do módulo tem lugar sempre que a informação contida no repositório de dados é insuficiente para o módulo gerar uma resposta. O desenvolvimento do modulo deve suportar adição de diferentes serviços e pedidos concorrentes aos mesmos, mitigando possível indisponibilidade ou altos tempos de espera. 

O funcionamento deste módulo assenta em informação geográfica contida na base de dados, por isso o repositório de dados deve conter informação necessária e válida para alimentar as consultas promovidas pelo módulo em questão.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.8\textwidth]{images/staticmap}
  \caption{Imagem estática com pontos de interesse}
  \label{fig:staticmap}
\end{figure}

% section Definição do Módulo (end)

\section{Mapas Locais} % (fold)
\label{sec:Mapas Locais}
O suporte de mapas locais faz parte da conceção do \emph{Mapping Module}, uma vez que lá reside uma vasta quantidade de informação necessária ao módulo. Todavia, a disponibilização de um serviço dedicado de mapas necessita de um servidor dedicado.

Para o projeto em questão considera-se esta como a opção mais viável, um serviço de mapas e direções local, afastado de qualquer limitação imposta por serviços externos, incluindo \emph{branding}. A criação deste serviço é possível com integração de plataformas \emph{open source} como \emph{OpenStreetMaps}~\footnote{\url{http://www.openstreetmap.org/}}, que contém todas as ferramentas necessárias para geração dos ~\emph{tiles} dos mapas, bem como informação geográfica associada. Como repositório de dados geográficos, \emph{PostGIS}~\footnote{\url{http://postgis.refractions.net/}}, extensão para \emph{PostgreSQL}~\footnote{\url{http://www.postgresql.org/}} que dota a base de dados de tipos de dados e funções necessárias para lidar com objetos geográficos. Uma vez integrada toda a solução, os respetivos \emph{tile} podem ser gerados através da ferramentas \emph{Mapnik}~\footnote{\url{http://mapnik.org/}}.

\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.5]{images/mapping_module}
  \caption{Esquema Mapping Module}
  \label{fig:mapping_module}
\end{figure}
% subsection Mapas Locais (end)

\section{\acl{API} (API)} % (fold)
\label{sec:api}
A descrição de comportamentos a cumprir pelo \emph{Mapping Module} está sujeita a exploração por parte dos grupos de trabalho. Nesta secção apenas se referem algumas ideias iniciais num formato informal aberto a discussão.

Numa análise preliminar sobre a \ac{API} disponibilizada e, uma vez que a tarefa principal do \emph{Mapping Module} é servir informação geográfica, para as necessidades enumeradas pondera-se a seguinte \emph{API} informal:

\begin{description}
  \item[Mapping(provider)]~\\
    O construtor da da classe que representa este módulo deve receber sempre um \emph{provider}, este haje como \emph{gateway} para acesso a serviços externos. O interface de implementação que o \emph{provider} deve cumprir ainda não se encontra especificado, contudo a estrutura preliminar encontra-se na Figura~\ref{fig:class_diagram}.

  \item[getImage(coordinates, width, height)]~\\
    Acede ao serviço definido pelo \emph{provider} e compila uma imagem com as respetivas dimensões e devolve a sua localização. A imagem é composta pelos tiles necessários para formar uma área do mapa, inclui também os pontos de interesse existentes na área requisitada. Os pontos de interesse são definidos por um pedido ao módulo incumbido de os decifrar, ou por uma pesquisa à base de dados, o raio pré-definido limita a pesquisa.

  \item[getTiles(coordinates, width, height)]~\\
    Executa a mesma tarefa que a operação anterior, no entanto, devolve apenas a localização dos tiles necessários para apresentar o mapa. O acesso aos tiles pode eventualmente ser limitado por serviços externos.

  \item[convertToSystem(coordinates)]~\\
    Meramente utilitário para conversão entre sistemas de coordenadas.    

  \item[getPointOfInterest(coordinates, radius)]~\\
    Devolve todos os pontos de interesse encontrados dentro de um determinado raio de pesquisa. 

  \item[getDirections(startCoordinates, endCoordinate)]~\\
    Encontra e devolve as direções necessárias para chegar de ponto \emph{A} a ponto \emph{B}. A informação sobre as direções está condicionada pelos dados existentes nos serviços, externos ou locais. 

  \item[getFormat(coordinates, radius)]~\\
    Gerar um ficheiro com formato útil para aplicações externas. Pode revelar-se útil um formato para importação ou exportação de dados.
\end{description}


\begin{figure}[ht]
  \centering
  \includegraphics[scale=0.6]{images/class_diagram}
  \caption{Diagrama de Classes}
  \label{fig:class_diagram}
\end{figure}
% section API (end)
