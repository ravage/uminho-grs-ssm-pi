CREATE OR REPLACE FUNCTION zero_spots() RETURNS VOID AS $$
DECLARE
i int;
rnd int;
max_parks int;
half_parks int;
BEGIN
  SELECT MAX(id) FROM parks INTO max_parks;
  half_parks := (max_parks / 2)::int;
  FOR i IN 1..half_parks LOOP
    rnd := random(1, max_parks)::int;
    UPDATE parks SET spots = 0 WHERE id = rnd;
  END LOOP;
END
$$ LANGUAGE plpgsql;
