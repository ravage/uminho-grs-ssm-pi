final = []

output = File.open(ARGV[1], 'w')

File.open(ARGV[0], 'r') do |f|
  while line = f.gets
    l = line.split('|')
    l.each do |item|
      item.strip!
      item = item.gsub(/'/, "''")
    end
    if l[0] == ''
      output << "INSERT INTO parks (name, lon, lat) VALUES (NULL, #{l[1]}, #{l[2]});\n"
    else
      output << "INSERT INTO parks (name, lon, lat) VALUES ('#{l[0]}', #{l[1]}, #{l[2]});\n"
    end
  end
end

output.close
