CREATE OR REPLACE FUNCTION type_tags(park_id bigint) RETURNS TEXT AS $$
DECLARE
  sql TEXT;
  tags TEXT;
  r RECORD;
BEGIN
  tags := '';
  
  sql := 'SELECT t.description FROM types t JOIN parks_types pt ON t.id = pt.type_id WHERE pt.park_id = ' || park_id; 
  FOR r IN EXECUTE sql LOOP
    RAISE NOTICE '%', r.description;
    tags := tags || r.description || ',';
  END LOOP;
  RETURN tags;
END
$$ LANGUAGE plpgsql;
