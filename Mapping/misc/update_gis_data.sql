﻿CREATE OR REPLACE FUNCTION update_gis_data() RETURNS TRIGGER AS $$
BEGIN
	IF (TG_OP = 'INSERT') THEN
		EXECUTE 'UPDATE ' 
			|| TG_TABLE_NAME
			|| ' SET coords = ST_SetSRID(ST_POINT('
			|| NEW.lon
			|| ', '
			|| NEW.lat
			|| '), 4326) WHERE id = '
			|| NEW.id;
	ELSE
		EXECUTE 'UPDATE ' 
			|| TG_TABLE_NAME
			|| ' SET coords = ST_SetSRID(ST_POINT('
			|| OLD.lon
			|| ', '
			|| OLD.lat
			|| '), 4326) WHERE id = '
			|| OLD.id;
	END IF;
	RETURN NULL;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION update_gis_data(relation text) RETURNS INTEGER AS $$
DECLARE
	r RECORD;
	counter INTEGER := 0;
BEGIN
	
	FOR r in EXECUTE 'SELECT * FROM ' || relation LOOP
		counter := counter + 1;
		EXECUTE 'UPDATE ' 
		|| relation
		|| ' SET coords = ST_SetSRID(ST_POINT('
		|| r.lon
		|| ', '
		|| r.lat
		|| '), 4326) WHERE id = '
		|| r.id;
	END LOOP;
	RETURN counter;
END
$$ LANGUAGE plpgsql;

DROP TRIGGER on_update_gis_data on parks;
CREATE TRIGGER on_update_gis_data AFTER INSERT ON parks
    FOR EACH ROW EXECUTE PROCEDURE update_gis_data();
