﻿DROP FUNCTION mapping_within(float, float, float, varchar, numeric, float);
DROP TYPE geopark;

CREATE TYPE geopark AS (
	id BIGINT,
	name VARCHAR(128),
	lon FLOAT,
	lat FLOAT,
	spots INT,
	coords TEXT,
  img VARCHAR(32),
  parkType VARCHAR(64),
  parkPrice NUMERIC(12, 4)
);


CREATE OR REPLACE FUNCTION mapping_within(lon float, lat float, radius float, park_type varchar(80) default null, price numeric(12, 4) default null, spots float default null) RETURNS SETOF geopark AS $$
DECLARE
	r geopark;
  sql text;
BEGIN	
  sql := 'SELECT DISTINCT ON (p.id)
          p.id, p.name, p.lon, p.lat, p.spots, ST_AsGeoJSON(p.coords) as coords,
          p.img, (SELECT type_tags(p.id)) as parkType, pp.price as parkPrice
          FROM parks p 
          LEFT JOIN parks_prices pp ON pp.park_id = p.id
          LEFT JOIN prices ON prices.id = pp.price_id
          LEFT JOIN parks_types pt ON pt.park_id = p.id
          LEFT JOIN types t ON pt.type_id = t.id '
          || 'WHERE ST_DWithin(coords, ST_SetSRID(ST_POINT(' 
		      || lon 
		      || ', '
		      || lat 
		      || '), 4326), '
		      || radius
		      || ', true)';

  IF park_type IS NOT NULL THEN
    sql := sql || ' AND t.description ILIKE  ''%' || park_type || '%'' ';
  END IF;
  
  IF price IS NOT NULL THEN
    sql := sql || ' AND pp.price <= ' || price;
  END IF;

  IF spots IS NOT NULL THEN
    sql := sql || ' AND p.spots >= ' || spots;
  END IF;

  FOR r IN EXECUTE sql LOOP
    RETURN NEXT r;
  END LOOP;
  RAISE NOTICE '%', sql; 
	--FOR r IN EXECUTE 'SELECT id, name, lon, lat, spots, ST_AsGeoJSON(coords) as coords FROM parks '
			--|| 'WHERE ST_DWithin(coords, ST_SetSRID(ST_POINT(' 
			--|| lon 
			--|| ', '
			--|| lat 
			--|| '), 4326), '
			--|| radius
			--|| ', true)' LOOP
		--RETURN NEXT r;
	--END LOOP;
	RETURN;
END

$$ LANGUAGE plpgsql;
