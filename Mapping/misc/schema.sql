CREATE TABLE parks (
  id          BIGSERIAL PRIMARY KEY,
  name        VARCHAR(80),
  location    VARCHAR(80),
  lon         FLOAT,
  lat         FLOAT,
  spots       INT DEFAULT 0,
  img         VARCHAR(150) DEFAULT 'defaultpark.jpg',
  inserted_at TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE parks_prices (
  park_id     BIGINT,
  price_id    BIGINT,
  price       NUMERIC(12, 4),
  inserted_at TIMESTAMP DEFAULT LOCALTIMESTAMP,
  PRIMARY KEY (park_id, price_id)
);

CREATE TABLE parks_types (
  type_id     BIGINT,
  park_id     BIGINT,
  inserted_at TIMESTAMP DEFAULT LOCALTIMESTAMP,
  PRIMARY KEY (type_id, park_id)
);


CREATE TABLE prices (
  id          BIGINT PRIMARY KEY,
  description VARCHAR(20),
  inserted_at TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE types (
  id          BIGSERIAL PRIMARY KEY,
  description VARCHAR(80),   
  inserted_at TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE users (
  id              BIGSERIAL PRIMARY KEY,
  username        VARCHAR(50) UNIQUE,
  password        VARCHAR(512),
  name            VARCHAR(80),
  address         VARCHAR(150),
  postcode        VARCHAR(12),
  city            VARCHAR(30),
  phone_landline  INT,
  phone_mobile    INT,
  email           VARCHAR(40),
  last_login      TIMESTAMP,
  active          BOOLEAN DEFAULT true,
  inserted_at     TIMESTAMP DEFAULT LOCALTIMESTAMP
);

CREATE TABLE users_preferences (
  user_id       BIGINT,
  type_id       BIGINT,
  min_spots     INT,
  max_price     NUMERIC(12,4),
  inserted_at   TIMESTAMP DEFAULT LOCALTIMESTAMP
);

SELECT AddGeometryColumn('public', 'parks', 'coords', 4326, 'POINT', 2);
