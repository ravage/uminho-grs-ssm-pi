var Mapping = (typeof Mapping == 'undefined') ? {} : Mapping;

Utils.namespace('Mapping.Page.Main');

Mapping.Page.Main = (function($) {	
	var page = {};
	
	var initializeNotifyBar = function() {
		//$.notifyBar({ html: "This is 'Notify bar'!" });
		$("#callGreen").click(function(){
			$.notifyBar({
				html: "Thank you, your settings were updated!",
				jqObject: $("#greenDiv")
			});
		});

		$("#callError").click(function(){
			$.notifyBar({
				jqObject: $("#errorDiv")
			});
		});

		$("#common").click(function(){
			$.notifyBar({});
		});

		var meta = $('meta[name=error]');
		if (meta.length != 0) {
			if (meta.attr('content') == 1) {
				$.notifyBar({ cls: "error", html: "password" });
			}
		}

		$("#success").click(function(){
			$.notifyBar({ cls: "success", html: "Your data has been changed!" });
		});

		$("#close").click(function(){
			$.notifyBar({ html: "Click 'close' to hide notify bar", close: true, delay: 1000000 });
		});
	};
	
	page.initialize = function() {
		Mapping.Page.initialize({
			tiles: "http://um.fragmentized.net/ers-pi/map/tiles/${z}/${x}/${y}.png",
			longitude: -8.3955603,
			latitude: 41.5607185,
			mapWrapper: 'map_canvas',
			zoom: 14
		}, true);

		var form = $('#formSearch');
		var radius = 300;  
		var templateRequest = new Mapping.Request('http://localhost/parkingsearch', 'text');
		
		form.submit(function (evt) { evt.preventDefault(); } );
		
		form.delegate('input[type=image]', 'click', function(evt) {
			evt.preventDefault();
			if (evt.currentTarget.id == 'geo-locate') {
				var address = form.find('#geo-address').val();
				var radius = form.find('#radius').val();
				
				$('ul.listing').fadeOut(function() {
					$(this).remove();
				});
				
				if (radius != '' && !isNaN(parseInt(radius, 10))) {
					Mapping.Page.setRadius(radius);
				}
				
				if (address == '') {
					Mapping.Page.searchFrom(form.serialize());
				}
				else {
					Mapping.Page.searchFrom(address, form.serialize());
				}
			}
			else if (evt.currentTarget.id == 'geo-watch') {
				Mapping.Page.watchPosition();
			}
			else if (evt.currentTarget.id == 'radius-increase') {
				Mapping.Page.setRadius(radius += 300);
			}
			else if (evt.currentTarget.id == 'radius-decrease') {
				Mapping.Page.setRadius(radius -= 300);
			}
		});
		
		$(document).bind('mapping:nolocation', function() {
			$.notifyBar({ cls: "error", html: "Não foi possível obter a sua localização! Experimente colocar uma morada na pesquisa." });
		});
		
		$(document).bind('mapping:gotparks', function(evt, parks) {
			var items = [];
			for (var i = 0, length = parks.length; i < length; i++) {
				var attributes = parks[i].attributes;
				attributes.name = attributes.name || 'Desconhecido';
				attributes.parkImage = (attributes.spots == 0) ? 'images/parkicon-full.gif' : 'images/parkicon.gif';
				attributes.parkType = attributes.parktype.split(',');
				attributes.parkType.pop();
				attributes.parkType = attributes.parkType.join(' | ');
				items.push(parks[i].attributes);
			}
			templateRequest.get('jst/park.jst?v=1', function(status, template) {
				var rendered = Mustache.to_html(template, {'items': items});
				$(rendered).hide().appendTo('#main').fadeIn();
			});
		});
	};
	
	return page;
}(jQuery));