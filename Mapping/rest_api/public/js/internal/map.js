// define project namespace
var Mapping = Mapping || {};

// define some layer types, currently only working on OSM
Mapping.LayerTypes = { 
  OSM               : "OSM",
  GOOGLE_HYBRID     : (typeof(google) === "undefined") ? 0 : google.maps.MapTypeId.HYBRID,
  GOOGLE_ROADMAP    : (typeof(google) === "undefined") ? 0 : google.maps.MapTypeId.ROADMAP,
  GOOGLE_SATTELITE  : (typeof(google) === "undefined") ? 0 : google.maps.MapTypeId.SATTELITE,
  GOOGLE_TERRAIN    : (typeof(google) === "undefined") ? 0 : google.maps.MapTypeId.TERRAIN
};

// object definning default projections
Mapping.Projections = {
  EPGS4326    : new OpenLayers.Projection("EPSG:4326"),
  EPGS900913  : new OpenLayers.Projection("EPSG:900913")
};

/*
Class: Map

Wraps around OpenLayers functionality
*/ 
Mapping.Map = (function() {

  // shortcut for Mapping.LayerTypes
  var LayerTypes = Mapping.LayerTypes;
  var Projections = Mapping.Projections;
  var Formatters = Mapping.Formatters;

  /*
    Constructor: Map

      Initializes Map class

    Parameters:

      mapWrapper - id from the div that will hold the map  
      options    - configuration options for the map display
  */ 
  var Map = function(mapWrapper, options) {
    this.mapWrapper = mapWrapper;

    Utils.setOptions(this.options, options); 

    var baseLayer = this.options.baseLayer;
    delete this.options["baseLayer"];

    var layer = this.createLayer(baseLayer.name, baseLayer.layerType, baseLayer);

    this.map = new OpenLayers.Map(mapWrapper, this.options);
    this.map.addLayer(layer);
  };

  Map.prototype = {

    options: {
      zoom: 10,
      projection: Projections.EPGS900913,
      displayProjection: Projections.EPGS4326,
      units: 'm',
      maxResolution: 156543.0339,
      maxExtent: new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508.34),
      controls: [
        new OpenLayers.Control.Navigation(),
        new OpenLayers.Control.LayerSwitcher(),
        new OpenLayers.Control.PanZoomBar(),
        new OpenLayers.Control.ScaleLine({geodesic: true}),
        new OpenLayers.Control.MousePosition()
      ],
      baseLayer: {
        layerType: Mapping.LayerTypes.OSM,
        name: "OSM Default Layer",
        numZoomLevels: 16
      }
    },

    createLayer: function(name, type, options) {
      var layer = null;
      var path = "";

      if (type == LayerTypes.OSM) {
        if (options.hasOwnProperty("tilePath")) {
          path = options["tilePath"];
          delete options["tilePath"];
          layer = new OpenLayers.Layer.OSM(name, path, options);
        }
        else {
          layer = new OpenLayers.Layer.OSM(name, "", options);
        }
      }
      //layer.projection = Projection.WGS84;
      return layer;
    },

    addLayer: function(layer) {
      this.map.addLayer(layer);
    },

    addLayers: function(layers) {
      this.map.addLayers(layers);
    },

    removeLayer: function() {

    },

    addControl: function(control) {
      this.map.addControl(control); 
    },

    addControls: function(controls) {
      this.map.addControls(controls);
    },

    removeControl: function() {

    },

    setCenter: function(longitude, latitude, zoom) {
      var point = this.createPoint(longitude, latitude);

      this.map.setCenter(point, zoom);
    },

    panTo: function(coords) {
      this.map.panTo(coords);
    },

    createPoint: function(longitude, latitude) {
      var point = new OpenLayers.LonLat(longitude, latitude);
      
      this.transform(point);

      return point;
    },
    
    transform: function(geometry) {
      if (this.map.projection != Projections.EPGS4326) {
        geometry.transform(Projections.EPGS4326, this.map.getProjectionObject());
      }
      return geometry;
    },

    addPopup: function(popup) {
      this.map.addPopup(popup);
    },

    removePopup: function(popup) {
      this.map.removePopup(popup);
    },

    getProjectionObject: function() {
      return this.map.getProjectionObject();
    },

    getMap: function() { return this.map; },

    createCircle: function(origin, radius) {
      var center = new OpenLayers.Geometry.Point(origin.lon, origin.lat);
      return this.createGeodesicPolygon(center, radius, 20, 45, Projections.EPGS900913);
    },
    /*
    * APIMethod: createGeodesicPolygon
    * Create a regular polygon around a radius. Useful for creating circles
    * and the like.
    *
    * Parameters:
    * origin - {<OpenLayers.Geometry.Point>} center of polygon.
    * radius - {Float} distance to vertex, in map units.
    * sides - {Integer} Number of sides. 20 approximates a circle.
    * rotation - {Float} original angle of rotation, in degrees.
    * projection - {<OpenLayers.Projection>} the map's projection
    */
    createGeodesicPolygon: function(origin, radius, sides, rotation, projection){

      if (projection.getCode() !== "EPSG:4326") {
        origin.transform(projection, new OpenLayers.Projection("EPSG:4326"));
      }
      var latlon = new OpenLayers.LonLat(origin.x, origin.y);

      var angle;
      var new_lonlat, geom_point;
      var points = [];

      for (var i = 0; i < sides; i++) {
        angle = (i * 360 / sides) + rotation;
        new_lonlat = OpenLayers.Util.destinationVincenty(latlon, angle, radius);
        new_lonlat.transform(new OpenLayers.Projection("EPSG:4326"), projection);
        geom_point = new OpenLayers.Geometry.Point(new_lonlat.lon, new_lonlat.lat);
        points.push(geom_point);
      }
      var ring = new OpenLayers.Geometry.LinearRing(points);
      return new OpenLayers.Geometry.Polygon([ring]);
    }
  };

  return Map;
})();
