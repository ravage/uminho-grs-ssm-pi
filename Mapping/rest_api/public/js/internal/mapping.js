// define project namespace
var Mapping = Mapping || {};


Mapping.Page = (function(){
  var page = {},
  map,
  templateRequest,
  youAreHere,
  searchArea,
  format,
  parks,
  previousClickedFeature,
  rc,
  Projections = Mapping.Projections,
  searchRadius = 300,
  geocoder,
  geolocation,
  select,
	currentLonLat = [0, 0];

  page.initialize = function(options) {
    var tilePath = options.tiles;

    var mapOptions = {
      zoom: 6,
      numZoomLevels: 16,
      baseLayer: {
        layerType: Mapping.LayerTypes.OSM,
        tilePath: tilePath,
        name: "OSM Default Layer"
      }
    };

    geolocation = new Mapping.Geolocation(true, geolocationSuccess, geolocationFailure);

    map = new Mapping.Map(options.mapWrapper, mapOptions);
    map.setCenter(options.longitude, options.latitude, options.zoom);

    format = new OpenLayers.Format.GeoJSON({
      'internalProjection': Projections.EPGS900913,
      'externalProjection': Projections.EPGS4326
    });

    var parkingStyle = Mapping.Styles.Parking;
    var style = new OpenLayers.Style(parkingStyle.style, parkingStyle.context);

    var cluster = new OpenLayers.Strategy.Cluster({
      distance: 25,
      threshold: 2 
    });

    parks = new OpenLayers.Layer.Vector('Car Parking', {
      styleMap: style,
      strategies: [cluster]
    });


    select = new OpenLayers.Control.SelectFeature(parks);
    parks.events.on({
      featureselected: parksFeatureSelecHandler,
      featureunselected: parksFeatureUnselectHandler
    });

    map.addControl(select);
/*
var street = new OpenLayers.Layer.Google(
"Google Street",
{numZoomLevels: 20}
);

var hybrid = new OpenLayers.Layer.Google(
"Google Hybrid", {
type: google.maps.MapTypeId.HYBRID, 
numZoomLevels: 20,
sphericalMercator: true
}
);

var bingHybrid = new OpenLayers.Layer.VirtualEarth("Bing Hybrid", { 
sphericalMercator: true,
type: VEMapStyle.Hybrid 
});

map.addLayers([street, hybrid, bingHybrid]);
*/
    searchArea = new OpenLayers.Layer.Vector('Search Area');

    youAreHere = new OpenLayers.Layer.Vector('You Are Here', {styleMap: style});

    map.addLayer(searchArea);
    map.addLayer(parks);
    map.addLayer(youAreHere);

    geocoder = new Mapping.Geocoder(geocodeSuccess, geocoderError);

    rc = new Mapping.Request('http://localhost:9292', 'json');
    templateRequest = new Mapping.Request('http://localhost:9292', 'text');

    geolocation.watchPosition();
  };
  
  page.watchPosition = function() {
    geolocation.watchPosition();
  };

  page.geocode = function(address) {
     geolocation.stopPositionWatch();
     geocoder.geocode(address);
  }; 
  
  page.setRadius = function(radius) {
    var feature = youAreHere.features[0].clone();
    searchRadius = (radius >= 100) ? radius : searchRadius;
    var json = new OpenLayers.Format.JSON({
      'internalProjection': Projections.EPGS900913,
      'externalProjection': Projections.EPGS4326
    });
    //geolocationSuccess(json.read((format.write(feature))));
  };

	page.searchFrom = function(address, query) {
		var geocoder = new Mapping.Geocoder(function(results) {
			var lon = results.geometry.coordinates[0];
			var lat = results.geometry.coordinates[1];
			var searchQuery = [query, 'lon='.concat(lon), 'lat='.concat(lat)].join('&');
			var point = map.createPoint(lon, lat);
			drawRadius(point, searchRadius);
			geolocationSuccess(results, searchQuery);
		}, geocoderError);
		
		if (typeof query == 'undefined') {
			if (currentLonLat[0] == 0 || currentLonLat[1] == 0) {
				$(document).trigger('mapping:nolocation');
				return;
			}
			var searchQuery = [address, 'lon='.concat(currentLonLat[0]), 'lat='.concat(currentLonLat[1])].join('&');
			var point = map.createPoint(currentLonLat[0], currentLonLat[1]);
			drawRadius(point, searchRadius);
			rc.get('api/parks.json', searchQuery, parksRequestHandler);
		}
		else {
			geocoder.geocode(address);
		}
	};

  var geocodeSuccess = function(results) {
		currentLonLat[0] = results.geometry.coordinates[0];
		currentLonLat[1] = results.geometry.coordinates[1];
		geolocationSuccess(results);
  };

  var geocoderError = function(status) {
  };

  var parksRequestHandler = function(status, data) {
		parks.removeAllFeatures();
		if (status == 'error' || data.status == 'empty') {
			log("parks request error");
		}
		else {
			var features = format.read(data);
    	parks.addFeatures(features);
    	select.activate();
			$(document).trigger('mapping:gotparks', [features]);
		}
  };

  var parksFeatureSelecHandler = function(evt) {
    if (previousClickedFeature) {
      parksFeatureUnselectHandler();
    }

    var feature = evt.feature;

    //FIXME templates should be cached once requested
    templateRequest.get('jst/popup.jst?v=1', function(status, data) {
      if (feature.cluster) {
        return;
      }
      
      var view = {};
      Utils.setOptions(view, feature.attributes);

      view.name = (view.name) ? view.name : 'Desconhecido';      

      view.lon  = OpenLayers.Util.getFormattedLonLat(view.lon, 'lon', 'dms');
      view.lat  = OpenLayers.Util.getFormattedLonLat(view.lat, 'lat', 'dms');

      var popup = new OpenLayers.Popup.Anchored(
        'popup',  
        feature.geometry.getBounds().getCenterLonLat(), 
        new OpenLayers.Size(300,100),
        Mustache.to_html(data, view), 
        null, true, parksFeatureUnselectHandler);

        //popup.autoSize = true;
        feature.popup = popup;
        map.addPopup(popup);
    });
    previousClickedFeature = feature;
  };

  var parksFeatureUnselectHandler = function(evt) {
    var feature = (evt && evt.feature) ? evt.feature : previousClickedFeature;

    if(feature.popup) {
      map.removePopup(feature.popup);
      feature.popup.destroy();
      delete feature.popup;
    }
  };   

  var geolocationSuccess = function(response, searchQuery) {
		currentLonLat[0] = response.geometry.coordinates[0];
		currentLonLat[1] = response.geometry.coordinates[1];
		
    var point = map.createPoint(response.geometry.coordinates[0], response.geometry.coordinates[1]);

    if (youAreHere.features.length > 0) {
      youAreHere.features[0].move(point);
      searchArea.features[0].move(point);
    }
    else {
      var feature = format.parseFeature(response);
      feature.attributes['icon'] = 'gfx/you.png';
      feature.attributes['name'] = 'You Are Here!';
      youAreHere.addFeatures([feature]);

    }
    
		drawRadius(point, searchRadius);
		
		point.transform(Projections.EPGS900913, Projections.EPGS4326);
		
		if (searchQuery) {
    	rc.get('api/parks.json', searchQuery, parksRequestHandler);
		}
		else {
			rc.get('api/parks.json', { lon: point.lon, lat: point.lat, radius: searchRadius }, parksRequestHandler);
		}
  };

	var drawRadius = function(point, radius) {
		searchArea.removeAllFeatures();
    var circle = map.createCircle(point, searchRadius);
    var fcircle = new OpenLayers.Feature.Vector(circle);
    searchArea.addFeatures([fcircle]);
    map.panTo(point);
	};

  var geolocationFailure = function(response) {
    log('geolocation error');
  };

  return page;
})();

Mapping.Styles = {};

Mapping.Styles.Parking = (function() {
  var parking = {};

  var contextNameHandler = function(feature) {
    if (feature.cluster) {
      return [feature.cluster.length, ' items agregados, aumente o zoom para visualizar'].join('');
    }

    if (feature.attributes && feature.attributes.name) {
      return (feature.attributes.name) ? feature.attributes.name : '';
    }
    return '';
  };

  var contextIconHandler = function(feature) {
    if (feature.attributes && feature.attributes.icon) {
      return feature.attributes.icon;
    }

    if (feature.cluster) {
      return 'gfx/cluster.png';
    }
  
    if (feature.attributes && feature.attributes.hasOwnProperty('spots')) {
			
      if (feature.attributes.spots <= 0) {
					return 'gfx/full.png';
      }

      if (feature.attributes.spots < 100) {
        return 'gfx/free.png';
      }
    }

    return 'gfx/parking.png';
  };

  var contextLabelHandler = function(feature) {
    if (feature.cluster)
      return '';

    if (feature.attributes) {
      if (feature.attributes.spots == 'undefined' || feature.attributes.spots == 'null') {
        return '';
      }

      if (feature.attributes.spots < 100) {
        return feature.attributes.spots;
      }
    }

    return '';
  };

  parking.style = {
    pointRadius: 18,
    externalGraphic: '${icon}',
    graphicTitle: '${name}',
    label: '${label}',
    labelYOffset: 3,
    fontFamily: '"Lucida Grande", "Lucida Sans Unicode", "Lucida Sans"',
    fontWeight: 'bold',
    labelAlign: 'center',
    fontColor: '#fff',
    fontSize: '12px'
  };

  parking.context = {
    context: {
      name: contextNameHandler,
      icon: contextIconHandler,
      label: contextLabelHandler
    }
  };
  return parking;
})();
