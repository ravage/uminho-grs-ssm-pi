var Mapping = (typeof Mapping == 'undefined') ? {} : Mapping;

Mapping.Geocoder = (function() {

  var Geocoder = function(successFn, errorFn) {
    var geocoder = new google.maps.Geocoder();

    this.successHandler = successFn || Utils.noop;
    this.errorHandler = errorFn || Utils.noop;

    // accessor methods
    this.getGeocoder = function() {
      return geocoder;
    };
  };

  var geocodeHandler = function(results, status) {
    var result = {
      type: 'Feature',
      geometry: { type: 'Point', coordinates: [] },
      properties: {}
    };

    if (status == google.maps.GeocoderStatus.OK) {
      var response = results[0];
      result.properties = { address: response.formatted_address };
      result.geometry.coordinates = [response.geometry.location.lng(), response.geometry.location.lat()];
      this.successHandler(result);
    }
    else {
      this.errorHandler();
    }
  };

  Geocoder.prototype = {
    geocode: function(address) {
      var geocoder = this.getGeocoder();
      geocoder.geocode({ address: address }, Utils.scope(this, geocodeHandler));
    },

    reverseGeocode: function(longitude, latitude) {
      var geocoder = this.getGeocoder();
      var latLng = new google.maps.LatLng(latitude, longitude);
      geocoder.geocode({ latLng: latLng }, Utils.scope(this, geocodeHandler));
    }
  };

  return Geocoder;
})();
