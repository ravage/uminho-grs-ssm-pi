var Mapping = (typeof Mapping == 'undefined') ? {} : Mapping;

Mapping.Geolocation = (function() {

  var Geolocation = function(haccuracy, geoSuccess, geoFailure) {
    var id = -1;
    var supported = (navigator.geolocation) ? true : false;
    var highAccuracy = haccuracy || false; 
    this.geoSuccessHandler = geoSuccess || Utils.noop;
    this.geoFailureHandler = geoFailure || Utils.noop; 
    
    // getters / setters
    this.isSupported = function() {
      return supported;
    };

    this.getHighAccuracy = function() {
      return highAccuracy;
    };

    this.setHighAccuracy = function(value) {
      highAccuracy = value;
    };

    this.setWatcherId = function(value) {
      id = value;
    };

    this.getWatcherId = function() {
      return id;
    };
  };

  
  var geolocatorSuccessHandler = function(position) {
    var result = {
      type: 'Feature',
      geometry: { type: 'Point', coordinates: [] },
      properties: {}
    };
    result.geometry.coordinates.push(position.coords.longitude);
    result.geometry.coordinates.push(position.coords.latitude);
    this.geoSuccessHandler(result);
  };

  var geolocatorFailureHandler = function(errorType, error) {
    this.geoFailureHandler(errorType, error);
  };

  Geolocation.prototype = {
    getPosition: function() {
      if (this.isSupported()) {
        var that = this;
        navigator.geolocation.getCurrentPosition(
          Utils.scope(this, geolocatorSuccessHandler),
          Utils.scope(this, geolocatorFailureHandler),
          { enableHighAccuracy: this.getHighAccuracy, maximumAge: 0 }
        );
      }
    },

    watchPosition: function() {
      if (this.isSupported()) {
        var id = navigator.geolocation.watchPosition(
          Utils.scope(this, geolocatorSuccessHandler), 
          Utils.scope(this, geolocatorFailureHandler),
          { enableHighAccuracy: this.getHighAccuracy }
        );
        this.setWatcherId(id);
      }
    },

    stopPositionWatch: function() {
      navigator.geolocation.clearWatch(this.getWatcherId());
    }
  };

  return Geolocation;
})();
