var Mapping = (typeof Mapping == 'undefined') ? {} : Mapping;
var Utils = (typeof Utils == 'undefined') ? {} : Utils;

Utils.noop = function() {};

Utils.namespace = function(ns) {
  var parent = Mapping,
  nsParent = 'Mapping';
  parts = ns.split('.');

  if (parts[0] == nsParent) {
    parts.splice(0, 1);
  }

  for (var i = 0, length = parts.length; i < length; i++) {
    if (typeof(parent[parts[i]]) === 'undefined') {
      parent[parts[i]] = {};
    }
  } 
  return parent;
};

Utils.scope = function(context, handle) {
  var args = Array.prototype.splice.call(arguments, 2);
  return function() {
    var argumentsArray = Array.prototype.slice.call(arguments);
    handle.apply(context, Array.prototype.concat.call(args, argumentsArray));
  };
};

/*
  Function: setOptions

    Merge properties recursively from two objects

  Parameters:
    
    defaultOptions  - Base object in which properties will be augmented
    newOptions      - Incoming object with the properties to merge into defaultOptions object
*/
Utils.setOptions = function(defaultOptions, newOptions) {
  for (option in newOptions) {
    if (typeof(newOptions[option]) == "object") {
      Utils.setOptions(defaultOptions[option], newOptions[option]);
    }
    defaultOptions[option] = newOptions[option];  
  }
};

Utils.trim = function(value) {
	return value.replace(/^\s*(\S*(?:\s+\S+)*)\s*$/, "$1");
};

// usage: log('inside coolFunc',this,arguments
// paulirish.com/2009/log-a-lightweight-wrapper-for-consolelog/
window.log = function(){
  log.history = log.history || [];
  log.history.push(arguments);
  if(this.console){
    console.log(Array.prototype.slice.call(arguments));
  }
};
