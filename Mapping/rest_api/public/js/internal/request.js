var Mapping = Mapping || {};

Mapping.Request = (function($) {
  var Request = function(url, format) {
    var _url = url;
    var _dataType = format;
    var _resource = null;
    
    // getters / setters
    this.getUrl = function() {
      return _url;
    };

    this.getDataType = function() {
      return _dataType;
    };

    this.setDataType = function(value) {
      _dataType = value;
    };

    this.getResource = function() {
      return _resource;
    };

    this.setResource = function(value) {
      _resource = value;
    };
  };

  Request.Events = {
    SUCCESS   : 'restclient:success',
    COMPLETE  : 'restclient:complete',
    ERROR     : 'restclient:error'
  };

  Request.prototype = (function() {
    var Events = Request.Events;
    
    //TODO Rethink event implementation....
    /*
    successHandler = function(c, data, textStatus, XMLHttpRequest) {
    $(c).trigger(Events.SUCCESS, [data]); 
    };

    errorHandler = function(XMLHttpRequest, textStatus, errorThrown) {
    $(context).trigger(Events.ERROR, [textStatus, errorThrown]); 
    };

    completeHandler = function(XMLHttpRequest, textStatus) {
    $(context).trigger(Events.COMPLETE, [textStatus]); 
    };

    dataFilterHandler = function(data, type) {
    if ((type == 'json' || type == 'jsonp') && data === '') {
    return '{}';
    }
    return data;
    };
    */
    
    var request = function(data, requestType, handler) {
      if (typeof data == 'function') {
        handler = data;
        data = {};
      }
      else {
        data = data || {};
      }

      handler = handler || Utils.noop;
      
      $.ajax({
        url: [this.getUrl(), '/', this.getResource()].join(''),
        dataType: this.getDataType(),
        data: data, 
        type: requestType,
        success: function(response, status, xhr) { handler(status, response); },
        error: function(xhr, status, error) { handler(status, error); }
      });
    };

    return { 
      get: function(resource, data, handler) {
        this.setResource(resource);
        request.call(this, data, 'GET', handler);
      },

      post: function(resource, data, handler) {
        this.setResource(resource);
        request.call(this, data, 'POST', handler);
      },

      put: function(resource, data, handler) {
        this.setResource(resource);
        request.call(this, data, 'PUT', handler);
      },

      'delete': function(resource, data, handler) {
        this.setResource(resource);
        request.call(this, data, 'DELETE', handler);
      }
      
      /*setResource: function(res) {
        this.resource = res;
      },

      setUrl: function(url) {
        this.url = url;
      },

      setX: function(v) {
        x = v;
      },

      getX: function() {
        return x;
      }*/
    };
  })();

  return Request;
})(jQuery);
