class Park < Sequel::Model(:parks)
  many_to_many :prices, :join_table => :parks_prices
  many_to_many :types, :join_table => :parks_types

  def self.nearby(search)
    radius = search[:radius] || 300.0
    park_type = search[:park_type] || nil
    max_price = search[:max_price] || nil
    min_spots = search[:min_spots] || nil

    data = DB.fetch('SELECT * from mapping_within(?, ?, ?, ?, ?, ?)', 
                      search[:lon], search[:lat], radius, park_type, max_price, min_spots)
    puts data.sql
    if data.count == 0
      return { :status => 'empty' }
    end
    
    geojson('coords', data)
  end

  def self.parks
    data = self.fetch('SELECT *, ST_AsGeoJSON(coords) as coords FROM parks'); 
    geojson('coords', data)
  end

  def self.geojson(gis_field, data)
    feature_collection = { :type => 'FeatureCollection', :features => [], :status => 'success' }
    for row in data
      feature = {}
      properties = {}

      feature[:type] = 'Feature'
      feature[:geometry] = JSON::parse(row[gis_field.to_sym])

      row.keys.each do |column|
        properties[column] = row[column] unless column.to_s == gis_field
      end

      feature[:properties] = properties
      feature_collection[:features] << feature
    end
    feature_collection
  end
end
