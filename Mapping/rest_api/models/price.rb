class Price < Sequel::Model(:prices)
  many_to_many :parks, :join_table => :parks_prices
end
