class Type < Sequel::Model(:types)
  many_to_many :parks, :join_table => :parks_types
  one_to_many :preferences, :class => :Preference
end
