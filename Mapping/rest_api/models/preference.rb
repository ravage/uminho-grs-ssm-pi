class Preference < Sequel::Model(:users_preferences)
  many_to_one :type
  many_to_one :user, :one_to_one => true
end
