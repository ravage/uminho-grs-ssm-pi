class Application < Sinatra::Base
  use Rack::JSONP

  set :static, true
  set :public, 'public'
  configure :development do
    register Sinatra::Reloader
    also_reload "models/*.rb"
  end
end
