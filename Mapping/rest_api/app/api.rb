# encoding: utf-8
class API < Application
  # /api/parks.format/?lon=0.00&lat=0.00
  # /api/parks.format/?lon=0.00&lat=0.00&radius=0
  # /api/parks.format
  get '/api/parks.:format' do
    pass unless params[:format] == 'json'

    content_type :json

    remove_empty(params)
    search = parse_query(%w{lon lat radius max_price min_spots park_type})
    
    puts search
    if !search.nil? && search.include?(:lon) && search.include?(:lat)
      Park.nearby(search).to_json
    else
      Park.parks.to_json
    end

  end

  private

  def parse_query(valid_params)
    search = {}

    params.each do |key, value| 
      begin
        search[key.to_sym] = (value.to_f == 0.0) ? value : value.to_f if valid_params.include?(key) 
      rescue
        return nil 
      end
    end
    return search
  end

  def remove_empty(hash)
    hash.each do |key, value|
      value = value.strip
      hash.delete(key) if value.empty?
    end
  end

end
