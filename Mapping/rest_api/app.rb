require 'rubygems'
require 'sequel'
require 'json'
require 'rack/contrib/jsonp'
require 'sinatra/base'
require 'sinatra/reloader'
require 'app/application'
require 'app/api'


DB = Sequel.connect('postgres://localhost/ers_pi')
Sequel::Model.plugin :json_serializer

require 'models/park'
require 'models/user'
require 'models/preference'
require 'models/price'
require 'models/type'
