# http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames

class Numeric
  def to_radians
    self * Math::PI / 180.0 
  end
end

=begin
n = 2 ^ zoom
xtile = ((lon_deg + 180) / 360) * n
ytile = (1 - (log(tan(lat_rad) + sec(lat_rad)) / π)) / 2 * n
=end
module Mapping

  class << self
    def long_to_x(long, zoom)
      ((long + 180.0) / 360.0 * (2.0 ** zoom)).floor
    end

    def x_to_long(x, zoom)
      x / (2.0 ** zoom) * 360.0 - 180;  
    end

    def lat_to_y(lat, zoom)
      ((1.0 - Math.log(Math.tan(lat.to_radians) + 1.0 / Math.cos(lat.to_radians)) / Math::PI) / 2.0 * (2.0 ** zoom)).floor
    end

    def y_to_lat(y, zoom)
      n = Math::PI - 2.0 * Math::PI * y / (2.0 ** zoom);
      180.0 / Math::PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n)));
    end

    def map_to_tile(lat, long, zoom) 
      Tile.new(x_to_tile(long, zoom), y_to_tile(lat, zoom), zoom)
    end

    def tile_to_bounding_box(x, y, zoom)
      BoundingBox bb = BoundingBox.new
      bb.north = y_to_lat(y, zoom)
      bb.south = y_to_lat(y + 1, zoom)
      bb.west = x_to_long(x, zoom)
      bb.east = x_to_long(x + 1, zoom)
      bb
    end
  end

  class Point
    attr_accessor :x, :y

    def initialize(x, y)
      @x = x
      @y = y
    end
  end

  class Coordinate
    attr_accessor :latitude, :longitude

    def initialize(lat, long)
      @latitude = lat
      @longitude = long
    end
  end

  class Tile < Point
    attr_accessor :zoom

    def initialize(x, y, zoom)
      super(x, y)
      @zoom = zoom
    end
  end

  class BoundingBox
    attr_accessor :north, :south, :east, :west
  end
end
=begin
n = 2 ^ zoom
lon_deg = xtile / n * 360.0 - 180.0
lat_rad = arctan(sinh(π * (1 - 2 * ytile / n)))
lat_deg = lat_rad * 180.0 / π
=end

  puts Mapping.long_to_x(10, 3)
  puts Mapping.lat_to_y(10,3)

  puts Mapping::Tile.new(1, 2, 3).x
